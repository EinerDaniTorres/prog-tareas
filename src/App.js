import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

// data
import { tareas } from './tareas.json';

// subcomponents
import TodoForm from './components/TodoForm';

class App extends Component {
  constructor() {
    super();
    this.state = {
      tareas
    }
    this.handleAddTodo = this.handleAddTodo.bind(this);
  }

  removeTodo(index) {
    this.setState({
      tareas: this.state.tareas.filter((e, i) => {
        return i !== index
      })
    });
  }

  handleAddTodo(todo) {
    this.setState({
      tareas: [...this.state.tareas, todo]
    })
  }

  render() {
    const tareas = this.state.tareas.map((todo, i) => {
      return (
        <div className="col-md-4" key={i}>
          <div className="card mt-4">
            <div className="card-title text-center">
              <h2>{todo.title}</h2>
              <span className="badge badge-pill badge-danger ml-2">
                {todo.priority}
              </span>
            </div>
            <div className="card-body">
              <h5>Responsable: {todo.responsible}</h5>
            </div>
            <div className="card-body">
              <h6>Tarea: </h6> {todo.description}
            </div>
            <div className="card-footer">
              <button
                className="btn btn-danger"
                onClick={this.removeTodo.bind(this, i)}>
                Borrar tarea
              </button>
            </div>
          </div>
        </div>
      )
    });

    // RETURN THE COMPONENT
    return (
      <div className="App">

        <nav className="navbar navbar-dark bg-primary">
          <a className="navbar-brand" href="/">
            Cantidad de tareas
            <span className="badge badge-pill badge-light ml-2">
              {this.state.tareas.length}
            </span>
          </a>
        </nav>

        <div className="container">
          <div className="row mt-4">

            <div className="col-md-4 text-center">
              <img src="LogoUniajc.jpg" alt="" width="300" height="300" />


              <TodoForm onAddTodo={this.handleAddTodo}></TodoForm>
            </div>

            <div className="col-md-8">
              <div className="row">
                {tareas}
              </div>

            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
